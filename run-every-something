#!/usr/bin/perl
# run-every-something
# Copyright (C) Eskild Hustvedt 2023
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
use v5.30;
use feature 'signatures';
use constant {
    HOUR => 3600,
    DAY => 86400,

    V_VERBOSE => 1,
    V_DEBUG => 2,
};
no warnings 'experimental::signatures';
use File::Path qw(mkpath);
use Getopt::Long;
use File::Basename qw(basename);

my $VERSION = '0.1';
my $verbosity = 0;

# Purpose: Prints a message if verbosity is >= $level
sub printv($level,$message)
{
    if ($level <= $verbosity)
    {
        my $levelPrefix = '';
        if ($level == V_DEBUG)
        {
            $levelPrefix = '[DEBUG] ';
        }
        say 'run-every-something: '.$levelPrefix.$message;
    }
}

# Purpose: Stupid assertion implementation. dies with $message if $value is falsy
sub assert ($value,$message)
{
    if (!$value)
    {
        die('Assertion failed: '.$message);
    }
}

# Purpose: Reads $file into a scalar and returns it. dies if $file does not exist.
sub slurp ($file)
{
    die("$file: does not exist\n") if ! -e $file;
    die("$file: is a directory\n") if -d $file;
    die("$file: is not writeable\n") if !-w $file;

    local $/ = undef;
    open(my $in,'<',$file) or die("Failed to open $file for reading: $!\n");
    my $content = <$in>;
    close($in) or warn("Failed to close filehandle referring to $file: $!\n");
    return $content;
}

# Purpose: Reads $file into a scalar and returns it. Returns $value if $file does not exist.
sub slurpOrReturn($file,$value)
{
    if (!-e $file)
    {
        printv(V_DEBUG,$file.' does not exist, returning '.$value);
        return $value;
    }
    return slurp($file);
}

# Purpose: Returns the path to the state file for the identifier $name
sub getStateFileFor($name)
{
    my $configDir = configDirectory();
    $name =~ s/\W//g;
    $name = 'state-'.$name;
    if(length $name > 200)
    {
        die("$name: too long state name\n");
    }
    return $configDir.'/'.$name;
}

# Purpose: Returns the path to our config directory
sub configDirectory ()
{
    my $base = $ENV{XDG_CONFIG_HOME} // $ENV{HOME}.'/.config';
    my $configDir = $base.'/run-every-something';
    if (!-d $configDir)
    {
        mkpath($configDir) or die("Failed to mkpath($configDir): $!\n");
    }
    return $configDir;
}

# Purpose: Writes $content to $file. dies if it can't.
sub spurt($file,$content)
{
    if (-e $file && ! -w $file)
    {
        die("$file: is not writeable\n");
    }
    die("$file: is a directory\n") if -d $file;
    open(my $out,'>',$file) or die("Failed to open $file for writing: $!\n");
    print {$out} $content;
    close($out) or warn("Failed to close filehandle referring to $file: $!\n");
}

# Purpose: Parses a time expression into components
# ie. "1 week 2 days" becomes [{value => 1, unit => 'week'},{value => 2, unit => 'days'}]
sub parseExpression($expression)
{
    my @entries;
    my $finalize = sub ($value,$unit) { 
        push(@entries,{
                value => int($value),
                unit => $unit
            });
    };

    my @content = split('',$expression);

    my $currentValue = '';
    my $currentUnit = '';

    foreach my $part (@content)
    {
        if ($part eq ' ')
        {
            next;
        }
        if ($part =~ /\d/)
        {
            if($currentUnit ne '')
            {
                $finalize->($currentValue,$currentUnit);
                $currentValue = '';
                $currentUnit = '';
            }

            $currentValue .= $part;
        }
        else
        {
            $currentUnit .= $part;
        }
    }
    $finalize->($currentValue,$currentUnit);
    return \@entries;
}

# Purpose: Interprets a part of an expression and returns a time value
#           (integer) in seconds corresponding to that time
# Ie. "resolveExpressionComponent(1,hour,'1hour')" returns 3600
#
# dies if it can't recognize $unit
sub resolveExpressionComponent($number,$unit,$expression)
{
    if ($unit eq 'week' || $unit eq 'w' || $unit eq 'weeks')
    {
        return $number * (DAY*7);
    }
    if ($unit eq 'day' || $unit eq 'd' || $unit eq 'days')
    {
        return $number * DAY;
    }
    if ($unit eq 'hour' || $unit eq 'h' || $unit eq 'hours')
    {
        return $number * HOUR;
    }
    die("Unhandled unit: '$unit' in expression '$expression'\n");
}

# Purpose: Resolves a full (string) time expression into a single time value
#          (integer) in seconds
sub resolveFullExpression($expression)
{
    my $components = parseExpression($expression);
    my $time = 0;
    foreach my $entry (@{ $components })
    {
        my $resolution += resolveExpressionComponent($entry->{value},$entry->{unit},$expression);
        $time += $resolution;
        printv(V_DEBUG,'Resolved '.$entry->{value}.$entry->{unit}.' to '.$resolution);
    }
    assert($time != 0,'time != 0');
    printv(V_DEBUG,'Resolved time period to '.$time);
    return $time;
}

# Purpose: Output usage information, exiting with $exitVal if it is defined
sub usage ($exitVal = undef)
{
    say "";
    say 'Usage: '.basename($0).' [<name>] [<duration-expression>] [<options>] -- [<command>]'."\n";

    print "\n";
    print "Options:\n";
    say "  -h,  --help         View this help screen";
    say "       --version      Output version information and exit";
    say "  -v,  --verbose      Increase verbosity (can be supplied multiple times)";

    if (defined $exitVal)
    {
        exit($exitVal);
    }

    return 1;
}

# Purpose: die, appending a message to see --help
sub helpfulDie($message)
{
    die($message."See `".basename($0)." --help` for more information.\n");
}

# Purpose: Checks if $time has passed since we last ran the command associated with $name
sub timeHasPassed($name,$time)
{
    my $lastRun = int(slurpOrReturn(getStateFileFor($name),0));
    printv(V_DEBUG,'Last ran: '.$lastRun);

    my $offset = 0;
    if ($time >= DAY)
    {
        $offset = HOUR;
    }
    else
    {
        $offset = 60*5;
    }

    if ( $lastRun+$time <= (time+$offset))
    {
        return 1;
    }
    return 0;
}

# Purpose: Run a command and update the time state file
sub runScheduledCommand($name,@command)
{
    my $time = time;

    printv(V_DEBUG,'Running: '.join(' ',@command));
    system(@command);
    my $ret = $? >> 8;

    if ($ret != 0)
    {
        printv(0,'Command returned '.$ret);
    }

    spurt(getStateFileFor($name),$time);
}

# Purpose: Handle a request for a named command to be run
sub handleRequest(@request)
{
    my $name = shift(@request);

    if (!defined $name || !length $name)
    {
        helpfulDie("Missing name, unable to continue\n");
    }

    my $expression = shift(@request);

    if (!defined $expression || !length $expression)
    {
        helpfulDie("Missing duration-expression, unable to continue\n");
    }
    my $time = resolveFullExpression($expression);

    if(scalar(@request) == 0)
    {
        helpfulDie("Missing command to run\n");
    }

    if (timeHasPassed($name,$time))
    {
        runScheduledCommand($name,@request);
    }
    else
    {
        printv(V_VERBOSE,'Not enough time has passed, not running command');
    }
}

# Purpose: Main entry point
sub main ()
{
    Getopt::Long::Configure('no_ignore_case','bundling');

    GetOptions(
        'h|help' => sub {
            usage(0);
        },
        'version' => sub
        {
            print 'run-every-something version '.$VERSION."\n";
            exit(0);
        },
        'v|verbose+' => \$verbosity,
    ) or die('See --help for more information'."\n");

    handleRequest(@ARGV);
}

main();
__END__

=encoding utf8

=head1 NAME

run-every-something - a cron helper for running commands based on arbitrary time periods

=head1 SYNOPSIS

B<run-every-something> [I<NAME>] [I<DURATION-EXPRESSION>] [I<OPTIONS>] -- [I<COMMAND TO RUN>]

=head1 DESCRIPTION

B<run-every-something> is a cron helper for arbitrary time periods. Add
B<run-every-something> to cron with the correct options and along with a command
you want it to run, and then B<run-every-something> will keep track of the
time since last execution and run the command when it's time. It's good at
things that cron isn't, like running a command every N weeks.

=head1 PARAMETERS

=over

=item B<NAME>

The name is an identifier for individual commands, which I<run-every-something>
uses internally to store when a command was last run. It should be a string,
and must be unique for the current user. If you use the same name for multiple
commands then they will run inconsistently since a single time is used to track
multiple things. If ~/.config/ is on a case-sensitive filesystem, B<NAME> will
be case sensitive.

Example: I<myWeeklyTask>.

=item B<DURATION-EXPRESSION>

An expression defining the duration between each time a command should be run.
See the section B<DURATION EXPRESSION> below for more information.

=item B<COMMAND TO RUN>

This is the complete command that you want to run. If you provide command-line
parameters to it, then you should add a I<--> before the command, so that
I<run-every-something> doesn't try to interpret the parameters (which will
likely cause it to fail).

=back

=head1 OPTIONS

=over

=item B<--help>

Display the help screen

=item B<--version>

Output the run-every-something version and exit

=item B<-v, --verbose>

Increase run-every-something verbosity. May be supplied multiple times to further increase
verbosity.

=back

=head1 DURATION EXPRESSION

The duration expression is a single string that represents the duration you
want between each execution. The string can optionally contain spaces, which
must be ignored. The syntax is I<duration> I<unit> where duration is a positive
integer and unit is one of the below.

Each unit has multiple names, and each name is aliases of each other. As an
example "4 weeks", "4w" and "4week" are all equivalent

Each expression must contain at least one combination of duration and unit. For
example, "1 week 2 days" is a valid expression, as is "1w2d" and "1week2day".

Internally, all components of the expression are added up, and the result of that
is compared to the store value on when the command ran last. If the time since then
is equal to or greater than the duration expression, the command will be run.

I<Note>: to account for I<run-every-something> not always being run at the same
time down to the second (ie. because of system load), there is some slack when
the comparisons are made. If the duration expression is equal to or exceeds one
day, then a slack of one hour is used. Otherwise a slack of five minutes is
used.

=over

=item B<weeks>, B<week>, B<w>

Number of weeks

=item B<days>, B<day>, B<d>

Number of days

=item B<hours>, B<hour>, B<h>

Number of hours

=back

=head1 EXAMPLES

Usually you will want to run I<run-every-something> through cron. Either as a
daily or weekly task (or more often if you're using hours).

=over

=item run-every-something my4Weeks "4 weeks" -- ~/bin/sendPushnotification

Runs the command ~/bin/sendPushnotification every 4 weeks.
I<run-every-something> uses "my4Weeks" as an identifier interally to track when
it has last been run.

The duration expression I<"4 weeks"> could also have been written as I<4w>,
I<4week> or even I<"28 days">.

=item run-every-something myArbitraryTask "2 weeks 4 days" -- ~/bin/performTask

Runs the command ~/bin/performTask every 2 weeks and 4 days (ie. every 18 days).
I<run-every-something> uses "myArbitraryTask" as an identifier interally to
track when it has last been run.

=back

=head1 HELP/SUPPORT

If you need additional help, please visit the website at
L<http://www.zerodogg.org/run-every-something>

=head1 BUGS AND LIMITATIONS

If you find a bug, please report it at L<http://www.zerodogg.org/run-every-something/bugs>

=head1 AUTHOR

B<run-every-something> is written by Eskild Hustvedt I<<code aatt zerodogg d0t org>>

=head1 FILES

=over

=item I<XDG_CONFIG_HOME/run-every-something>

The run-every-something configuration directory. XDG_CONFIG_HOME is an
environment variable specified by the XDG Base Directory Specification. The
default value for XDG_CONFIG_HOME (and the value on most systems) is ~/.config,
so on most systems this will be I<~/.config/run-every-something>.

=item I<XDG_CONFIG_HOME/run-every-something/state-*>

The state files that are used to track when a command has last run. The
filenames will be I<state-> followed by the I<NAME> that was specified to run
the command (possibly normalized to remove whitespace and special characters).
The files all contain a single integer, which is the Unix time of when the
command was last run.

=back

=head1 LICENSE AND COPYRIGHT

Copyright (C) Eskild Hustvedt 2023

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.
