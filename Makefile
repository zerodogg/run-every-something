test:
	perl -c run-every-something
README.md: run-every-something
	pod2markdown run-every-something|perl -p -E 's/^#/##/; s/## NAME/# run-every-something/; s/^(##\s+)(.+)/$$1.ucfirst(lc($$2))/e; s/^run-every-something - a cron helper/a cron helper/; s/\~/\\~/g;' > README.md
