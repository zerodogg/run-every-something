# run-every-something

a cron helper for running commands based on arbitrary time periods

## Synopsis

**run-every-something** \[_NAME_\] \[_DURATION-EXPRESSION_\] \[_OPTIONS_\] -- \[_COMMAND TO RUN_\]

## Description

**run-every-something** is a cron helper for arbitrary time periods. Add
**run-every-something** to cron with the correct options and along with a command
you want it to run, and then **run-every-something** will keep track of the
time since last execution and run the command when it's time. It's good at
things that cron isn't, like running a command every N weeks.

## Parameters

- **NAME**

    The name is an identifier for individual commands, which _run-every-something_
    uses internally to store when a command was last run. It should be a string,
    and must be unique for the current user. If you use the same name for multiple
    commands then they will run inconsistently since a single time is used to track
    multiple things. If \~/.config/ is on a case-sensitive filesystem, **NAME** will
    be case sensitive.

    Example: _myWeeklyTask_.

- **DURATION-EXPRESSION**

    An expression defining the duration between each time a command should be run.
    See the section **DURATION EXPRESSION** below for more information.

- **COMMAND TO RUN**

    This is the complete command that you want to run. If you provide command-line
    parameters to it, then you should add a _--_ before the command, so that
    _run-every-something_ doesn't try to interpret the parameters (which will
    likely cause it to fail).

## Options

- **--help**

    Display the help screen

- **--version**

    Output the run-every-something version and exit

- **-v, --verbose**

    Increase run-every-something verbosity. May be supplied multiple times to further increase
    verbosity.

## Duration expression

The duration expression is a single string that represents the duration you
want between each execution. The string can optionally contain spaces, which
must be ignored. The syntax is _duration_ _unit_ where duration is a positive
integer and unit is one of the below.

Each unit has multiple names, and each name is aliases of each other. As an
example "4 weeks", "4w" and "4week" are all equivalent

Each expression must contain at least one combination of duration and unit. For
example, "1 week 2 days" is a valid expression, as is "1w2d" and "1week2day".

Internally, all components of the expression are added up, and the result of that
is compared to the store value on when the command ran last. If the time since then
is equal to or greater than the duration expression, the command will be run.

_Note_: to account for _run-every-something_ not always being run at the same
time down to the second (ie. because of system load), there is some slack when
the comparisons are made. If the duration expression is equal to or exceeds one
day, then a slack of one hour is used. Otherwise a slack of five minutes is
used.

- **weeks**, **week**, **w**

    Number of weeks

- **days**, **day**, **d**

    Number of days

- **hours**, **hour**, **h**

    Number of hours

## Examples

Usually you will want to run _run-every-something_ through cron. Either as a
daily or weekly task (or more often if you're using hours).

- run-every-something my4Weeks "4 weeks" -- \~/bin/sendPushnotification

    Runs the command \~/bin/sendPushnotification every 4 weeks.
    _run-every-something_ uses "my4Weeks" as an identifier interally to track when
    it has last been run.

    The duration expression _"4 weeks"_ could also have been written as _4w_,
    _4week_ or even _"28 days"_.

- run-every-something myArbitraryTask "2 weeks 4 days" -- \~/bin/performTask

    Runs the command \~/bin/performTask every 2 weeks and 4 days (ie. every 18 days).
    _run-every-something_ uses "myArbitraryTask" as an identifier interally to
    track when it has last been run.

## Help/support

If you need additional help, please visit the website at
[http://www.zerodogg.org/run-every-something](http://www.zerodogg.org/run-every-something)

## Bugs and limitations

If you find a bug, please report it at [http://www.zerodogg.org/run-every-something/bugs](http://www.zerodogg.org/run-every-something/bugs)

## Author

**run-every-something** is written by Eskild Hustvedt _&lt;code aatt zerodogg d0t org_>

## Files

- _XDG\_CONFIG\_HOME/run-every-something_

    The run-every-something configuration directory. XDG\_CONFIG\_HOME is an
    environment variable specified by the XDG Base Directory Specification. The
    default value for XDG\_CONFIG\_HOME (and the value on most systems) is \~/.config,
    so on most systems this will be _\~/.config/run-every-something_.

- _XDG\_CONFIG\_HOME/run-every-something/state-\*_

    The state files that are used to track when a command has last run. The
    filenames will be _state-_ followed by the _NAME_ that was specified to run
    the command (possibly normalized to remove whitespace and special characters).
    The files all contain a single integer, which is the Unix time of when the
    command was last run.

## License and copyright

Copyright (C) Eskild Hustvedt 2023

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
